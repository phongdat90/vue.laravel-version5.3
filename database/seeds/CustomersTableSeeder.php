<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Factory::create();
        foreach (range(1, 500) as $i) {
            \App\Models\Customer::create([
                'company' => $faker->company,
                'email'   => $faker->email,
                'name'    => $faker->name,
                'phone'   => $faker->phoneNumber,
                'address' => $faker->address,
            ]);
        }
    }
}
