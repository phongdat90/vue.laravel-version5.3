<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Support\FilterPaginateDataTable;
class Customer extends Model
{
    //
    protected $table = 'customers';
    protected $fillable = ['name', 'email', 'company', 'address', 'phone'];
    protected $filter = ['id', 'name', 'email', 'company', 'address', 'phone', 'created_at'];
    protected $operators = [
        'equal_to'     => '=',
        'less_than'    => '<',
        'greater_than' => '>',
        'like'         => 'LIKE',
        'in'           => 'IN',
        'not_in'       => 'NOT_IN',
        'between'      => 'BETWEEN'
    ];
    use FilterPaginateDataTable;
    public static function initialize() {
        return [
          'name' => '', 'company' => '', 'email' => '', 'address' => '', 'phone' => ''
        ];
    }
}
