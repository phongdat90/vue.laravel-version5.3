<?php
namespace App\Support;
use Validator;
use Log;
trait FilterPaginateDataTable {
    public function scopeFilterPaginateDataTable($query, $request) {
        $v = Validator::make($request->all(),[
            'search_operator' => 'required|in:'.implode(',', array_keys($this->operators)),
            'search_column'   => 'required|in:'.implode(',', $this->filter),
            'search_query_1'  => 'max: 255',
            'per_page'        => 'required|integer|min:1',
            'page'            => 'required|integer|min:1',
            'sort_column'     => 'required|in:'.implode(',', $this->filter),
            'direction'       => 'required|in:asc,desc'
        ]);
        if($v->fails()) {
            Log::error("Danger Error: Fail to login #File: "  . __FILE__ . " #Line: "  . __LINE__);
            dd($v->messages());
        }

       return $query->orderBy($request->sort_column, $request->direction)
           ->where(function($query) use ($request){
                if($request->has('search_query_1')) {
                    $this->builQuery(
                        $request->search_column,
                        $request->search_operator,
                        $request,
                        $query
                    );
                }
           })
           ->paginate($request->per_page);
    }
    protected function builQuery($column, $operator, $request, $query) {
        switch ($operator) {
            case 'equal_to':
            case 'less_than':
            case 'greater_than':
                $query->where($column, $this->operators[$operator], $request->search_query_1);
                break;
            case 'like':
                $query->where($column, 'like', '%'.$request->search_query_1.'%');
                break;
            case 'in':
                $query->whereIn($column, explode(',', $request->search_query_1));
                break;
            case 'not_in':
                $query->whereNotIn($column, explode(',', $request->search_query_1));
                break;
            case 'between':
                $query->whereBetween($column,
                    [
                      $request->search_query_1,
                      $request->search_query_2
                    ]);
                break;
            default:
                break;
        }
        return $query;
    }
}