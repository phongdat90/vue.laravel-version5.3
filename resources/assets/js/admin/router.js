import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', component: require('./customer/index.vue')},
        {path: '/admin/customer', component: require('./customer/index.vue')},
        {path: '/admin/customer/create', component: require('./customer/form.vue')},
        {path: '/admin/customer/:id', component: require('./customer/show.vue')},
        {path: '/admin/customer/:id/edit', component: require('./customer/form.vue'), meta: {mode: 'edit'}},
        {path: '/admin/invoice', component: require('./invoice/index.vue')},
        {path: '*', component: require('./errors/page-not-found.vue')}
    ]
});
export default router