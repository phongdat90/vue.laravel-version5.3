<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/admin/css/master.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>;
        //window.baseUrl = "<?php //echo config('app.baseUrl')?>";
        //console.log(window.Laravel)
    </script>
</head>
<body>
    <div class="container">
        <div id="app">
           {{--render code vuejs--}}
        </div>
    </div>
    <script src="/admin/js/master.js"></script>
</body>
</html>